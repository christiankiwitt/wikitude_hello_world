var World = {

    init: function initFn() {

        //initialze the tracking-engine
        this.targetCollectionResource = new AR.TargetCollectionResource("assets/wtc/marker.wtc", {});
        this.imageTracker = new AR.ImageTracker(this.targetCollectionResource, {});

        this.registeredIndicator = new AR.HtmlDrawable({ 
            html: "<div style='border: 24px solid green; border-bottom: 24px solid green; padding-top: 100%'></div>" }, 1, {
        });

        this.helloWorldLabel = new AR.Label("Hello World", 0.3, {
            "translate": {
            "y": 0.0,
            "z": 1.0,
            "x": 0.0
            },
            "rotate": {
            "z": 0.0,
            "x": 270.0,
            "y": 0.0
            },
            "style": {
            "backgroundColor": "#FFFFFF",
            "textColor": "#000000"
            }
            });
       
        var marker = new AR.ImageTrackable( this.imageTracker, 
                                            "marker_gen_base", 
                                            {
                                                drawables: {
                                                    cam: [this.registeredIndicator, this.helloWorldLabel]
                                                }
                                            });

    }
};

World.init();