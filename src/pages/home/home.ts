import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  wikitudePlugin: any;
  requiredFeatures = ["2d_tracking"];
  startupConfiguration = {
    "camera_position": "back"
  };
  architectWorldUrl = "www/assets/experience/world/wikitude_hello_world/index.html";

  constructor(public platform: Platform) {
    platform.ready().then(() => {
      this.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
      this.wikitudePlugin._sdkKey = "0pidk2suVB+66IiT3SIb39ONyzSxVckaDAtVgtOYQWr8bzdJPgUEb4TWSoKry7XKtoJm0BsDrNb16TWC8d5cWQUSKlKcRZ1GPJvuNRdYeOyBO/8XMKgdJcqQ3S6kiOp2ImemfOA/567+tsPs+gqd6DTZz7e4JE6LCmRiDKEPcA5TYWx0ZWRfXxKn+mbdI3w9+bFjCwmPADgC7YP1Ed/fKfYCZKJ5ktK0KSTvUcsOxV9JKs5qbuLnffCLi/1KGhayxb58zO4iJZ9XuM4wGzzZSR9wltCAC4UOdgz23U8C0bouWYUC9kV6f+6Tx7h990v64wBqwMgYFypIf4aLklAetY5c7lghCW+FUGKtjnLxhVMLhJjBFrPxYClyLrhS48ySVKdZvU9CJUJzVKCGKdVwyyLj3uZDgx8XTjkppTYH3RXNOkbKYITbq4w0mlT50Kh2IfeNiRfXX29rpg4bpIeNoWmUxWSovmIRjdI2mwuEg686ilsuNzkKpfsFE3bHGYKp/MRuhZ2SGiG8z63pevM+dV8mrK9PEFdwBzWZwt0Q8SSD3uZbLKWeGz1Pg1bFgFirVyML189tEYqADPufBg5NVcyWdRCvWZInamAGaP91yDRlmTEOwPktR+tb2A9pHEJfZNEcrzX43Xybjouwt87dn2CV8XtyfusesKSD08y1A7w0a/3UukwUhfclRj3IIpFBylgBgiEnpnC625fh8D+/Gw=="
    });
  }
  
  startWikitudeAR() {
    this.wikitudePlugin.isDeviceSupported(this.onDeviceSupported, this.onDeviceNotSupported, this.requiredFeatures);
  }

  onDeviceSupported = () => {
    this.wikitudePlugin.loadARchitectWorld(
      this.onARExperienceLoadedSuccessful,
      this.onARExperienceLoadError,
      this.architectWorldUrl,
      this.requiredFeatures,
      this.startupConfiguration
    );
  }

  onDeviceNotSupported() {
    console.log("Device Not Supported");
  }

  onARExperienceLoadError (err) {
    console.log('error load', err)
  }

  onARExperienceLoadedSuccessful () {
    console.log('success')
  }
}
